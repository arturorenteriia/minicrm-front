import { RouteConfig, Route, NavigationGuardNext } from 'vue-router';
import store from '../store/general/state';

const ifAuthenticated = (to:Route, from:Route, next:NavigationGuardNext,) => {

  if (store.login.isLogged) {
      next();
      return
  }
  next('/login')
}


const routes:  Array<RouteConfig> = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ],
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/users',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', component: () => import('pages/users/usersComponent.vue') },
      { path: 'create', component: () => import('pages/users/usersFormComponent.vue') },
      { path: 'edit/:id', component: () => import('pages/users/usersFormComponent.vue') }
    ],
    beforeEnter: ifAuthenticated
  },
  {
    path: '/clients',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'clients',component: () => import('pages/clients/clients.vue') },
      { path: 'profile', name: 'profile',component: () => import('pages/clients/profile.vue') },
      { path: 'profile/:id', name: 'editProfile',component: () => import('pages/clients/profile.vue') },
      { path: 'status/:status', name: 'searchbystatus',component: () => import('pages/clients/clients.vue') },

    ],
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/agents',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'agents',component: () => import('pages/agents/agents.vue') },
    ],
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/login',
    component: () => import('pages/login/login.vue'),
    name:'login',
  },
  {
    path: '/categories',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'categories',component: () => import('pages/categories/CategoriesTable.vue') },
    ],
    beforeEnter: ifAuthenticated,
  },
  {
    path: '/recycle-bin',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '/', name: 'recyble-bin',component: () => import('pages/recycleBin/recycleBin.vue') },
    ],
    beforeEnter: ifAuthenticated,
  },
  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
];

export default routes;
