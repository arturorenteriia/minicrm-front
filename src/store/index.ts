import { store } from 'quasar/wrappers';
import Vuex from 'vuex';
import general from './general';
 import { GeneralStateInterface } from './general/state';
/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */
//var general = require('./general');
export interface StateInterface {
  // Define your own store structure, using submodules if needed
  general: GeneralStateInterface;
  // Declared as unknown to avoid linting issue. Best to strongly type as per the line above.

}
export default store(function ({ Vue }) {
  Vue.use(Vuex);

  const Store = new Vuex.Store<StateInterface>({
    modules: {
      general
    },
    mutations: {
      login(state) {
          state.general.login.isLogged = true;
      },
      logout(state) {
          state.general.login.isLogged = false;
      },
      saveUserData(state, payload) {
          state.general.user.name = payload.name;
          state.general.user.lastName = payload.last_name;
          state.general.user.id = payload.id;
          state.general.user.role = payload.roles[0];
      },
      loading(state, payload) {
          state.general.loading = payload;
      },
      test(state){
        console.log(state);
      }
  },


    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: !!process.env.DEV
  });

  return Store;
});
