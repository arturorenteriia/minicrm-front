import { MutationTree } from 'vuex';
import { GeneralStateInterface } from './state';

const mutation: MutationTree<GeneralStateInterface> = {
  hostname ( state: GeneralStateInterface, url: string ) {
    state.apiUrl = url;
  },
  fronthostname ( state: GeneralStateInterface, url: string ) {
    state.frontUrl = url;
  },
  assets ( state: GeneralStateInterface, url: string ) {
    state.apiAssets = url;
  },
  loading ( state: GeneralStateInterface, status: boolean ) {
    state.loading = status;
  },
  statuses ( state: GeneralStateInterface, statuses: object ) {
    state.statuses = statuses;
  },

};

export default mutation;
