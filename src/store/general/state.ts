export interface GeneralStateInterface {
  login: {
    isLogged: boolean
  };
  loading: boolean;
  apiUrl: string;
  apiAssets: string;
  user:{
    name: string,
    lastName: string,
    user_id: number
    role: number
    id: number
  };
  statuses: object,
  frontUrl: string,

}

const state: GeneralStateInterface = {
  login:{
    isLogged: false,
    },
    loading: false,
    //apiUrl: 'https://codigogourmet.com/api/api',
    apiUrl: 'http://login_backend.test/api',
    apiAssets: 'https://codigogourmet.com/crm/api/minicrm/public/uploads',
    user: {
        name: '',
        lastName: '',
        user_id: 0,
        role: 0,
        id:0,
    },
    frontUrl: '',
    statuses: [],

};

export default state;
