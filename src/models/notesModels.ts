export interface propsInterface  {
    allNotes: object
    bgColor: string,
    client_id: string,
  };

  export interface singleNoteInterface  {
    createdBy: string
    note: string,
    date?: string,
    id?: number,
  };
