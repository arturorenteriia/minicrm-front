export interface filtersInterface  {
    show: boolean
    field: string,
    dateFrom: string,
    dateTo: string
    statusID: number
  };

