import { ref, computed, SetupContext, reactive } from "@vue/composition-api";
import axiosMethods from "./axiosMethodsComponent";
import { date } from "quasar";

export default function dashboardMethods(context: SetupContext) {
  /*
   *Manejador de erorres de axios INDISPENSABLE USAR
   */
  const { errorHandler } = axiosMethods(); //variable manejador errores

  /**
   * Te trae los ultimos 50 clientes agregados
   */
  const getData = () => {
    return new Promise<{statuses:[],favorites:[]}>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .get(context.root.$store.state.general.apiUrl + "/auth/clients/dashboard", {})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          resolve(response.data.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };


  /* const counterComputed = computed(() => {
        if (counter.value === 0) return counter.value;
        return counter.value > 0 ? `+${counter.value}` : `${counter.value}`;
    });*/
  return { getData, };
}
