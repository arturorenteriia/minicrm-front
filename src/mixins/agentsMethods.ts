import { ref, computed, SetupContext, reactive } from "@vue/composition-api";
import axiosMethods from "./axiosMethodsComponent";
import { agentInterface } from "./../models/agentsModels";
import { date } from "quasar";

export default function agentsMethods(context: SetupContext) {
  /*
   *Manejador de erorres de axios INDISPENSABLE USAR
   */
  const { errorHandler } = axiosMethods(); //variable manejador errores

  /**
   * Te trae los ultimos 50 clientes agregados
   */
  const getData = () => {
    return new Promise<[]>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .get(context.root.$store.state.general.apiUrl + "/auth/agents", {})
        .then((response) => {
          context.root.$store.commit("general/loading", false);

          console.log(response.data);
          resolve(response.data);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };


  /*
   *AGREGA EL AGENT A LA DB
   */
  const addAgent = (data:object) => {
    return new Promise<[]>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/agents", {data:data})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          if (response.data.success) {
            context.root.$q.notify({
              type: "positive",
              message: response.data.message
            });
          }
          console.log(response.data);
          resolve(response.data);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /*
   *ACTUALIZA EL AGENT A LA DB
   */
  const updateAgent = (data:agentInterface) => {
    return new Promise<[]>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .put(context.root.$store.state.general.apiUrl + "/auth/agents/" + data.id, {data:data})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          if (response.data.success) {
            context.root.$q.notify({
              type: "positive",
              message: response.data.message
            });
          }
          console.log(response.data);
          resolve(response.data);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /*
   *ELIMINA EL AGENT A LA DB
   */
  const deleteAgent = (id:number) => {
    return new Promise<[]>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .delete(context.root.$store.state.general.apiUrl + "/auth/agents/" + id)
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          if (response.data.success) {
            context.root.$q.notify({
              type: "positive",
              message: response.data.message
            });
          }else {
            context.root.$q.notify({
              type: "negative",
              message: response.data.message
            });
          }
          console.log(response.data);
          resolve(response.data);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /* const counterComputed = computed(() => {
        if (counter.value === 0) return counter.value;
        return counter.value > 0 ? `+${counter.value}` : `${counter.value}`;
    });*/
  return { getData, addAgent, updateAgent, deleteAgent };
}
