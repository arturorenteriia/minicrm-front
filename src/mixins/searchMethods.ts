import { ref, computed, SetupContext, reactive } from "@vue/composition-api";
import axiosMethods from "./axiosMethodsComponent";
import { date } from "quasar";
import { filtersInterface } from "../models/searchModels";

export default function searchMethods(context: SetupContext) {
  /*
   *Manejador de erorres de axios INDISPENSABLE USAR
   */
  const { errorHandler } = axiosMethods(); //variable manejador errores

  /**
   * Te trae los ultimos 50 clientes agregados
   */
  const getData = () => {
    return new Promise<Array<
      {
        id: 0,
        name: "",
        color_code: "",
      }
    >>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .get(context.root.$store.state.general.apiUrl + "/auth/statuses", {})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
         resolve(response.data.data);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        }
        )
        .catch((error) => {

          errorHandler(error, context);
          context.root.$store.commit("general/loading", false);
        });
    });
  };

  /**
   * agrega el cliente a favoritos, o lo quita
   */

  const markeSearch = (filters:filtersInterface) => {
    return new Promise<object>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(
          context.root.$store.state.general.apiUrl + "/auth/clients/search",
          { data: filters }
        )
        .then(response => {
          resolve(response.data.data);
          context.root.$store.commit("general/loading", false);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        }
        )
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        return  errorHandler(error, context);
        });
    });
  };

  /* const counterComputed = computed(() => {
        if (counter.value === 0) return counter.value;
        return counter.value > 0 ? `+${counter.value}` : `${counter.value}`;
    });*/
  return { getData, markeSearch };
}
