import { ref, computed } from "@vue/composition-api";
export default function composableSimpleCounter() {
    const errorHandler = (error, context) => {
        let code;
        let dataErrors;
        if (error.hasOwnProperty('response')) {
            code = error.response.status;
            dataErrors = error.response.data;

            switch (code) {
                case 422:
                    listErrorMesages(dataErrors, context)
                    break;
                default:
                    context.root.$q.notify({
                        type: 'negative',
                        message: dataErrors.message,
                    })
            }

        } else {
            context.root.$q.notify({
                type: 'negative',
                message: error.message,
            })
        }
    };

    const listErrorMesages = (dataErrors, context) => {
        const errors = dataErrors.errors;
        var htmlError = "<ul>";
        for (let error in errors) {
            console.log(errors[error]);
            htmlError += "<li>" + errors[error] + "</li>";
        }
        htmlError += "</ul>";
        context.root.$q.notify({
            type: 'negative',
            message: htmlError,
            html: true
        })
        return
    };

    return { errorHandler, listErrorMesages };
}