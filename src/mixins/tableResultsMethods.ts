import { ref, computed, SetupContext, reactive } from "@vue/composition-api";
import axiosMethods from "./axiosMethodsComponent";
import { rejects } from "assert";
import { date } from "quasar";

export default function tableResultsMethods(context: SetupContext) {
  /*
   *Manejador de erorres de axios INDISPENSABLE USAR
   */
  const { errorHandler } = axiosMethods(); //variable manejador errores

  /**
   * Te trae los ultimos 50 clientes agregados
   */
  const getData = () => {
    return new Promise<[]>((resolve, reject) => {
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .get(context.root.$store.state.general.apiUrl + "/auth/clients", {})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          console.log(response.data);
          resolve(response.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
    /**
   * Te trae los clientes eliminados
   */
     const getTrash = () => {
      return new Promise<[]>((resolve, reject) => {
        context.root.$store.commit("general/loading", true);
        context.root.$axios
          .get(context.root.$store.state.general.apiUrl + "/auth/clients/trash", {})
          .then((response) => {
            context.root.$store.commit("general/loading", false);
            console.log(response.data);
            resolve(response.data);
          })
          .catch((error) => {
            context.root.$store.commit("general/loading", false);
          });
      });
    };
  /*
   * trae el ultimo updated del cliente la fecha y el nombre de client form, payments, files and notes
   */
  const lastUpdate = (row:{
    updated_at: string;
    user_updated:{name:string}
    payments: [{ updated_at: string, user_updated:{name:string}}];
    notes_order_updated: [{ updated_at: string, user_updated:{name:string} }];
    files_order_updated: [{ updated_at: string, user_updated:{name:string} }];
  }) => {
    let data = new Array();
      let profile = {
        date: new Date(row.updated_at.toString()),
        user: row.user_updated.name
      };
      data.push(profile);


      if (row.payments.length > 0) {
        let payment = {
          date: new Date(row.payments[0].updated_at.toString()),
          user:  row.payments[0].user_updated.name
        };
        data.push(payment);
      }

      if (row.notes_order_updated.length > 0) {
        let note = {
          date:new Date(row.notes_order_updated[0].updated_at.toString()),
          user: row.notes_order_updated[0].user_updated.name
        };
        data.push(note);
      }
      if (row.files_order_updated.length > 0) {
        let file = {
          date: new Date(row.files_order_updated[0].updated_at.toString()),
          user: row.files_order_updated[0].user_updated.name,
        };
       data.push(file);
      }
      let sorted = data.sort((a, b) => a.date > b.date ? -1 : a.date < b.date ? 1 : 0);
      return sorted;
  };
  /**
   * agrega el cliente a favoritos, o lo quita
   */

  const addToFavorites = (row:{id:number, favorites:boolean}) => {
    return new Promise<boolean>((resolve, reject) => {
    //  context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/clients/favorites", {id: row.id})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          resolve(response.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /**
   * aelimina permanentemente
   */

   const deletePermanently = (row:{id:number}) => {
    return new Promise<boolean>((resolve, reject) => {
    //  context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/clients/deletepermanently", {id: row.id})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          resolve(response.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /**
   * restaura el cliente
   */

   const restore = (row:{id:number}) => {
    return new Promise<boolean>((resolve, reject) => {
    //  context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/clients/restore", {id: row.id})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          resolve(response.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  
/**
 * TRAE LOS CLIENTES DEPENDIENDO EL STATUS QUE VIENE EN LA RUTA WEB
 * @param row
 */
  const getClientsByStatus = (status:string) => {
    return new Promise<[]>((resolve, reject) => {
    //  context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/clients/getClientsByStatus", {status: status})
        .then((response) => {
          context.root.$store.commit("general/loading", false);
          resolve(response.data.data);
        })
        .catch((error) => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };

  /* const counterComputed = computed(() => {
        if (counter.value === 0) return counter.value;
        return counter.value > 0 ? `+${counter.value}` : `${counter.value}`;
    });*/
  return { getData, lastUpdate, addToFavorites,getClientsByStatus, getTrash, deletePermanently, restore };
}
