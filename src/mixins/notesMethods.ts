import { ref, computed, SetupContext, reactive } from "@vue/composition-api";
import axiosMethods from "./axiosMethodsComponent";
import { propsInterface, singleNoteInterface } from "../models/notesModels";
import { rejects } from "assert";
export default function notesMethods(context: SetupContext) {
  /*
   *Manejador de erorres de axios INDISPENSABLE USAR
   */
  const { errorHandler } = axiosMethods(); //variable manejador errores

  /**
   * Te trae los comentarios y el color para el header del cliente $id
   */
  const getData = (id: string) => {
    const data = {
      notes: ref(new Object()),
      color: ref(new Object())
    };
    context.root.$store.commit("general/loading", true);
    context.root.$axios
      .get(context.root.$store.state.general.apiUrl + "/auth/notes/" + id, {})
      .then(
        response => {
          console.log(response.data.data);
          data.notes.value = response.data.data.notes;
          data.color.value = response.data.data.color.color_code;
          context.root.$store.commit("general/loading", false);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        }
      )
      .catch(error => {
        context.root.$store.commit("general/loading", false);
      });
    console.log(data);
    return data;
  };
  /*
   * Agrega una nota al cliente
   */
  const addNote = async (id: string, note: string) => {
    return new Promise<object>((resolve, reject) => {
      var notes = "";
      context.root.$store.commit("general/loading", true);
      context.root.$axios
        .post(context.root.$store.state.general.apiUrl + "/auth/notes", {
          note: note,
          id: id
        })
        .then(
          response => {
            if (response.data.success) {
              context.root.$q.notify({
                type: "positive",
                message: response.data.message
              });
            }
            context.root.$store.commit("general/loading", false);
            resolve(response.data.data.notes);
          },
          error => {
            context.root.$store.commit("general/loading", false);
            return errorHandler(error, context);
          }
        )
        .catch(error => {
          context.root.$store.commit("general/loading", false);
        });
    });
  };
  /** 
   * Actualiza la nota
  */
 const updateNote = async (note:string, id:number | undefined) => {
  return new Promise<boolean>((resolve, reject) => {
    context.root.$store.commit("general/loading", true);
    context.root.$axios
      .put(context.root.$store.state.general.apiUrl + "/auth/notes/"+ id, {
        note: note,
      })
      .then(
        response => {
          if (response.data.success) {
            context.root.$q.notify({
              type: "positive",
              message: response.data.message
            });
          }
          context.root.$store.commit("general/loading", false);
          resolve(true);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        }
      )
      .catch(error => {
        context.root.$store.commit("general/loading", false);
      });
  });
};
 /** 
   * Elimina la nota
  */
 const deleteNote = async (id:number | undefined) => {
  return new Promise<object>((resolve, reject) => {
    context.root.$store.commit("general/loading", true);
    context.root.$axios
      .delete(context.root.$store.state.general.apiUrl + "/auth/notes/"+ id, {
      })
      .then(
        response => {
          if (response.data.success) {
            context.root.$q.notify({
              type: "positive",
              message: response.data.message
            });
          }
          context.root.$store.commit("general/loading", false);
          console.log(response.data.data.notes);
          resolve(response.data.data.notes);
        },
        error => {
          context.root.$store.commit("general/loading", false);
          return errorHandler(error, context);
        }
      )
      .catch(error => {
        context.root.$store.commit("general/loading", false);
      });
  });
};
  /* const counterComputed = computed(() => {
        if (counter.value === 0) return counter.value;
        return counter.value > 0 ? `+${counter.value}` : `${counter.value}`;
    });*/
  return { getData, addNote, updateNote, deleteNote };
}
