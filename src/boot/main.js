// import something here
import axios from "axios";
import { Cookies } from "quasar";
// "async" is optional
//ingresar el dominio principal de la api en produccion
var hostname = "http://index06.com/api/public/api";
var assets = "http://index06.com/api/public/uploads";
var fronthostname = "http://index06.com";

export default async ({
  Vue,
  app,
  store,
  router /* app, router, Vue, ... */
}) => {
  // something to do
  //verifica si trabajas en produccion, si no, entonces el dominio lo cambia por localhost
  if (location.hostname === "localhost" || location.hostname === "127.0.0.1") {
    //aqui cambia tu domnio local de la api
    hostname = "http://crm.test/api";
    assets = "http://crm.test/uploads";
    fronthostname = "http://localhost:8080";
    store.commit("general/hostname", hostname);
    store.commit("general/fronthostname", fronthostname);
    store.commit("general/assets", assets);
  } else {
    store.commit("general/hostname", hostname);
    store.commit("general/fronthostname", fronthostname);
    store.commit("general/assets", assets);
  }
  var value = Cookies.get("token");
  var date = Cookies.get("expiredDate");
  store.commit("loading", true);
  console.log("token " + new Date(date) + " " + new Date());
  if (value !== undefined && value !== null && new Date(date) > new Date()) {
    axios.interceptors.request.use(
      config => {
        config.headers.Accept = "application/json";
        config.headers.Authorization = "Bearer " + value;
        return config;
      },
      function(error) {
        return Promise.reject(error);
      }
    );
    axios
      .get(store.state.general.apiUrl + "/auth/getDataUser", {}, {})
      .then(responseUser => {
        store.commit("loading", false);
        store.commit("saveUserData", responseUser.data);
      })
      .catch(error => {
        router.push({ path: "/login" });
        store.commit("loading", false);
      });
    store.commit("login");
    router.push({ path: "/" });
  } else {
    Cookies.remove("token");
    Cookies.remove("expiredDate");
    console.log(router);
    location.href = "/#/login";
    store.commit("loading", false);
  }
};
